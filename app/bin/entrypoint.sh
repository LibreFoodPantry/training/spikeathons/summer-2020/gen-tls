#!/bin/sh


main(){
  cd "${TLS_HOME}"

  if [ ! -e __etc ] ; then
    echo "Copying initial configuration..."
    cp -a /app/etc __etc
  fi

  if [ ! -e __ca ] ; then
    gen_ca_files
  fi

  if [ ! -e __ia ] ; then
    gen_ia_files
  fi

  while [ -n "$1" ] ; do
    gen_service_files "$1"
    shift
  done

  echo "Done."
}


gen_ca_files(){
  echo "Generating certificate authority files..."
  mkdir -p __ca

  openssl genrsa -out __ca/key 4096

  openssl req -new -x509 \
    -days 1826 \
    -key __ca/key \
    -out __ca/crt \
    -config __etc/ca.cnf \
    -subj "$(cat __etc/ca.subject)"
}


gen_ia_files(){
  echo "Generating intermediate certificate authority files..."
  mkdir -p __ia

  openssl genrsa -out __ia/key 4096

  openssl req -new \
    -key __ia/key \
    -out __ia/csr \
    -config __etc/ca.cnf \
    -subj "$(cat __etc/ia.subject)"
  
  openssl x509 -sha256 -req \
    -days 730 \
    -in __ia/csr \
    -CA __ca/crt \
    -CAkey __ca/key \
    -set_serial 01 \
    -out __ia/crt \
    -extfile __etc/ca.cnf \
    -extensions v3_ca
  
  cat __ca/crt __ia/crt > __ia/pem
  cp __ia/pem __ca/pem
}


gen_service_files(){
  echo "Generating files for ${1}..."
  mkdir -p "$1"

  KEY="$1/key"

  openssl genrsa -out "$1/key" 4096
  chmod g+r "$1/key"

  openssl req -new \
    -key "$1/key" \
    -out "$1/csr" \
    -config __etc/service.cnf \
    -subj "$(cat __etc/service.subject)/OU=$1/CN=$1"

  openssl x509 -sha256 -req \
    -days 365 \
    -in "$1/csr" \
    -CA __ia/crt \
    -CAkey __ia/key \
    -CAcreateserial \
    -out "$1/crt" \
    -extfile __etc/service.cnf \
    -extensions v3_req

  cat "$1/crt" "$1/key" > "$1/pem"
  chmod o= "$1/pem"
  
  cp __ca/pem "$1/ca.pem"
}


main "$@"
