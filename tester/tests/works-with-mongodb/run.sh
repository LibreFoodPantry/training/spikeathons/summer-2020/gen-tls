#!/bin/sh

export TLS_HOME="${PWD}/tls"
mkdir -p "${TLS_HOME}"
docker-compose -f "${PROJECT_HOME}/app/docker-compose.yml" run --rm gen-tls client server

docker-compose -f "${PROJECT_HOME}/tester/tests/works-with-mongodb/docker-compose.yml" up --detach server
sleep 10
docker-compose -f "${PROJECT_HOME}/tester/tests/works-with-mongodb/docker-compose.yml" run --rm client
if [ ! $? ] ; then
  echo "FAILED: openssl-can-verify"
fi
docker-compose -f "${PROJECT_HOME}/tester/tests/works-with-mongodb/docker-compose.yml" down
rm -rf "${TLS_HOME}"
