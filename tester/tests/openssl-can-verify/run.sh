#!/bin/sh

export TLS_HOME="${PWD}/tls"
mkdir -p "${TLS_HOME}"
docker-compose -f "${PROJECT_HOME}/app/docker-compose.yml" run --rm gen-tls some-service

apk add --no-cache openssl
openssl verify -CAfile "${TLS_HOME}/some-service/ca.pem" "${TLS_HOME}/some-service/crt"
if [ ! $? ] ; then
  echo "FAILED: openssl-can-verify"
fi

rm -rf "${TLS_HOME}"
