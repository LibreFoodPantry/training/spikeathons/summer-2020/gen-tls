#!/bin/sh

cd "${PROJECT_HOME}"

for a_test in tester/tests/* ; do
  if [ -f "${a_test}/run.sh" ] ; then
    "${a_test}/run.sh"
  fi 
done
