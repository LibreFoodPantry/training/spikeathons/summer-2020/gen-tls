# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.14](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.13...v2.0.14) (2020-06-12)

### [2.0.13](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.12...v2.0.13) (2020-06-11)

### [2.0.12](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.11...v2.0.12) (2020-06-09)

### [2.0.11](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.10...v2.0.11) (2020-06-09)

### [2.0.10](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.9...v2.0.10) (2020-06-06)


### Bug Fixes

* print publish commands ([9d8761c](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/commit/9d8761c660a55fd31dbe6e4d44636582211bb3ef))

### [2.0.9](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.8...v2.0.9) (2020-06-06)

### [2.0.8](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.7...v2.0.8) (2020-06-06)


### Bug Fixes

* more tweaks ([5a49521](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/commit/5a49521220b1084a09fa562090c6e0da7dda8122))

### [2.0.7](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.6...v2.0.7) (2020-06-06)


### Bug Fixes

* tweaks ([9ab30e0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/commit/9ab30e02a8718f7b30f2dc460530206afd8bf762))

### [2.0.6](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.5...v2.0.6) (2020-06-06)

### [2.0.5](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.4...v2.0.5) (2020-06-06)

### [2.0.4](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.3...v2.0.4) (2020-06-06)

### [2.0.3](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.2...v2.0.3) (2020-06-06)

### [2.0.2](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.1...v2.0.2) (2020-06-06)

### [2.0.1](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.0...v2.0.1) (2020-06-05)

### [2.0.1](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v2.0.0...v2.0.1) (2020-06-05)

## [2.0.0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/compare/v1.0.0...v2.0.0) (2020-06-01)


### ⚠ BREAKING CHANGES

* Remove world read access from service pem.
This file contain the service's private key. So the pem should
have the same permissions as those on the private key. Now they do.

### remove

* world read access from service pem ([f6047fe](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/commit/f6047fe386734dff75bdc20ea0019482eb8707e4))

## 1.0.0 (2020-06-01)


### ⚠ BREAKING CHANGES

* The interface has changed dramatically. See README.md.

* complete rework ([91801a0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls/commit/91801a0c5ea81e7c68bb579258e7287901491329))
