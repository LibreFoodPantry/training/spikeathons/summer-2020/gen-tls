# tls

`tls` is a Docker container that generates the keys and certificates necessary for encrypted TLS (transport layer security) communication between Docker containers.

## Use gen-tls through Docker Compose

Create a docker-compose file named `gen-tls.yml` with the following contents.

```yml
version: "3.8"
services:
  gen-tls:
    image: registry.gitlab.com/librefoodpantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls:latest
    environment:
      - TLS_HOME
    volumes:
      - "${TLS_HOME}:${TLS_HOME}"
```

Then run the following to generate files for some-service and another-service.

```yml
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls some-service another-service
```

This generates the following files.

```bash
tls
├── __ca
│   ├── crt
│   ├── key
│   └── pem
├── __etc
│   ├── ca.cnf
│   ├── ca.subject
│   ├── ia.subject
│   ├── service.cnf
│   └── service.subject
├── __ia
│   ├── crt
│   ├── crt.srl
│   ├── csr
│   ├── key
│   └── pem
├── another-service
│   ├── ca.pem
│   ├── crt
│   ├── csr
│   ├── key
│   └── pem
└── some-service
    ├── ca.pem
    ├── crt
    ├── csr
    ├── key
    └── pem
```

`__ca` and `__ia` contain the certificate authority's files, `__etc` contains configuration files, and `some-service` and `another-service` contain each service's files.

Mount each service's directory into its container by adding a volumes entry in its section in a docker-compose file.

```yml
services:
  some-service:
    volumes:
      - "${PWD}/tls/some-service:/tls:ro"
  another-service:
    volumes:
      - "${PWD}/tls/another-service:/tls:ro"
```

With the above volumes entries, each service will have access its files, namely

- /tls/ca.pem: The CA's and intermediaries certificates, used for verifying certificates.
- /tls/pem: This service's PEM file containing its private key and certificate.
- /tls/key: This service's private key.
- /tls/crt: This service's X-509 certificate.
- /tls/csr: This service's request file (probably not needed).

## Use gen-tls through Docker

```bash
TLS_HOME="${PWD}/tls" docker run -e TLS_HOME -v "${PWD}/tls:${PWD}/tls" --rm registry.gitlab.com/librefoodpantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls:latest service1 service2
```

## Rekey a service

`tls` generates the CA's files only one the first run. To rekey a specific service, rerun tls on that service and `tls` will overwrite that service's files.

```yml
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls some-service
```

You may need to restart some-service.

## Rekey the CA and all services

Delete the directory containing `tls`'s data and run `tls` on all services again.

```yml
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls some-service another-service
```
